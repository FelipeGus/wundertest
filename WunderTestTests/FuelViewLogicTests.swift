//
//  FuelViewLogicTests.swift
//  WunderTestTests
//
//  Created by Felipe Figueiredo on 2/12/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import XCTest
import CoreGraphics
import simd

@testable import WunderTest

class FuelViewLogicTests: XCTestCase {

    var fuelView: FuelView!
    
    override func setUp() {
        fuelView = FuelView(frame: .init(x: 0, y: 0, width: 100, height: 20))
    }

    override func tearDown() {
        fuelView = nil
    }

    func testCorrectTextInformation() {
        fuelView.configureFor(amount: 30)
        XCTAssertEqual(fuelView.amountLabel.text, "\(R.string.localizable.fuelAmount()): 30\\100",
            "Error on fuel description")
    }
    
    func testColorWhenMaxFuel() {
        fuelView.configureFor(amount: 100)
        guard let backViewColor = fuelView.backgroundView.backgroundColor else {
            XCTFail("No colour was found")
            return
        }
        XCTAssertTrue(twoColorsDistanceLessThan(color1: backViewColor, color2: UIColor(red: 128, green: 192, blue: 0),
                                                maxDistance: 0.1))
    }
    
    func testColorWhenMinFuel() {
        fuelView.configureFor(amount: 0)
        guard let backViewColor = fuelView.backgroundView.backgroundColor else {
            XCTFail("No colour was found")
            return
        }
        XCTAssertTrue(twoColorsDistanceLessThan(color1: backViewColor, color2: UIColor(red: 255, green: 64, blue: 0),
                                                maxDistance: 0.1))
    }
    
    func testCorrectWidthOfColoredBackgroundFuelView() {
        fuelView.configureFor(amount: 35)
        fuelView.layoutIfNeeded()
        let backView = fuelView.backgroundView
        let width = backView.frame.width
        XCTAssertTrue((34...36).contains(Int(width)), "")
    }
    
    func getF3VectorForColor(color: UIColor) -> float3 {
        guard let components = color.cgColor.components else {
            XCTFail("Failed to retrieve components from color")
            fatalError()
        }
        var floatComponents = components.map({ Float($0)})
        floatComponents.removeLast()
        return float3(floatComponents)
    }
    
    func twoColorsDistanceLessThan(color1: UIColor, color2: UIColor, maxDistance: Float) -> Bool {
        let point1 = getF3VectorForColor(color: color1)
        let point2 = getF3VectorForColor(color: color2)
        let distanceValue = distance(point1, point2)
        return distanceValue < maxDistance
    }

}
