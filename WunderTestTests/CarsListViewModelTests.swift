//
//  CarsListViewModelTests.swift
//  WunderTestTests
//
//  Created by Felipe Figueiredo on 2/12/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import XCTest
@testable import WunderTest

class MockCarsDataLoader: CarsDataProvider {
    
    var response: Result<[Car]> = Result.okay(data: nil)

    func carsInformation(completion: @escaping (Result<[Car]>) -> Void) {
        completion(response)
    }
}

class MockLocationProvider: LocationProvider {
    var coordinates: LatLong?
}

class CarsListViewModelTests: XCTestCase {
    
    var viewModel: CarsListViewModel!
    var mockDataLoader: MockCarsDataLoader!
    var mockLocationProvider: MockLocationProvider!
    
    override func setUp() {
        mockDataLoader = MockCarsDataLoader()
        mockLocationProvider = MockLocationProvider()
        viewModel = CarsListViewModel(dataProvider: mockDataLoader, locationProvider: mockLocationProvider)
    }

    override func tearDown() {
        viewModel = nil
        mockDataLoader = nil
    }

    func testUpdateLocationByLocationProvider() {
        guard let cars: [Car]? = TestsJsonLoaderHelper.loadJson(named: "two_locations") else {
            XCTFail("Problem reading json")
            return
        }
        mockDataLoader.response = Result.okay(data: cars)
        viewModel.fetchCarsInformation()
        mockLocationProvider.coordinates = cars?.last?.coordinatesPair
        expectation(forNotification: LocationManager.updateLocationNotification, object: nil, handler: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            NotificationCenter.default.post(name: LocationManager.updateLocationNotification, object: nil)
        }
        waitForExpectations(timeout: 2)
        viewModel.provideCar(indexPath: [0, 0]) { (car) in
            XCTAssert(car.vin == cars?.last!.vin)
        }
    }
    
    func defaultJsonLoading() {
        guard let cars: [Car]? = TestsJsonLoaderHelper.loadJson(named: "five_locations") else {
            fatalError("Error while setting up for tests")
        }
        mockDataLoader.response = Result.okay(data: cars)
        viewModel.fetchCarsInformation()
    }
    
    func testQueryFilteringExpectedResults() {
        defaultJsonLoading()
        viewModel.query = "Hamburg"
        XCTAssertEqual(viewModel.numberOfRows(), 5)
        viewModel.query = "Lesser"
        XCTAssertEqual(viewModel.numberOfRows(), 1)
        viewModel.query = "sahdfjahsd"
        XCTAssertEqual(viewModel.numberOfRows(), 0)
    }
    
    func testQueryFilteringEmptyString() {
        defaultJsonLoading()
        viewModel.query = "Hamburg"
        viewModel.query = ""
        XCTAssertEqual(viewModel.numberOfRows(), 5)
        viewModel.query = nil
        XCTAssertEqual(viewModel.numberOfRows(), 5)
    }
}
