//
//  MemoryTests.swift
//  WunderTestTests
//
//  Created by Felipe Figueiredo on 2/12/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import XCTest
@testable import WunderTest

class MemoryTests: XCTestCase {

    var services: Services!
    
    override func setUp() {
        services = Services(serviceTypes: [.carsAPI],
                            factory: ProductionServiceFactory()
        )
    }

    func testCarsCoordinatorNotLeaking() {
        let navigationController = UINavigationController(rootViewController: UIViewController())
        var carsCoordinator: CarsCoordinator? = CarsCoordinator(
            navigationController: navigationController,
            services: services
        )
        carsCoordinator?.start()
        let ref = RefContainer(object: carsCoordinator)
        carsCoordinator = nil
        XCTAssertNil(ref.ref)
    }
    
    func testViewControllersNotLeaking() {
        let viewControllerFactory = CarsViewControllerFactory(services: services,
                                                              delegate: MockCarsDelegate()
        )
        var viewControllers = [CarsViewControllerFactory.Blueprint.carsList, .carsMap]
            .map(viewControllerFactory.createViewControllerFor)
        var containers: [RefContainer<UIViewController>] = []
        for viewC in viewControllers {
            let container = RefContainer(object: viewC)
            containers.append(container)
        }
        viewControllers = []
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            for container in containers {
                XCTAssertNil(container.ref,
                             "\(String(describing: container.ref?.classForCoder))")
            }
        }
        sleep(1)
    }
    
    func testCarsViewModelsNotLeaking() {
        var viewModels: [AnyObject] = [CarsListViewModel(dataProvider: MockCarsDataLoader()),
                          CarsMapViewModel(dataProvider: MockCarsDataLoader())]
        
        var containers: [RefContainer<AnyObject>] = []
        
        for viewM in viewModels {
            let container = RefContainer(object: viewM)
            containers.append(container)
        }
        viewModels = []
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            for container in containers {
                XCTAssertNil(container.ref,
                             "\(String(describing: container.ref?.classForCoder))")
            }
        }
        sleep(1)
    }
}

private final class RefContainer<T: AnyObject> {
    weak var ref: T?
    init(object: T?) {
        self.ref = object
    }
}

class MockCarsDelegate: CarsModuleActionsDelegate {
    var navigationController: UINavigationController = UINavigationController()
    
    func showCarsMap() {
    }
}
