//
//  CarsDataLoaderTests.swift
//  WunderTestTests
//
//  Created by Felipe Figueiredo on 2/12/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import XCTest
@testable import WunderTest

class CarsDataLoaderTests: XCTestCase {

    var dataLoader: CarsDataLoader!
    
    override func setUp() {
        dataLoader = CarsDataLoader()
    }

    override func tearDown() {
    }
    
    func testIfFetchWhenNoStorage() {
        dataLoader.storage = nil
        let fetchFromRemoteExpectation = expectation(description: "Fetch from remote")
        dataLoader.carsInformation { (result) in
            switch result {
            case .okay:
                fetchFromRemoteExpectation.fulfill()
            default:
                XCTFail("Failed to fetch from remote")
            }
        }
        wait(for: [fetchFromRemoteExpectation], timeout: 0.5)
    }
    
    func testIfValuesAreBeingSavedToStore() {
        let mockStorage = MockStorage()
        dataLoader.storage = mockStorage
        let fetchFromRemoteExpectation = expectation(description: "Fetch from remote")
        dataLoader.carsInformation { (result) in
            switch result {
            case .okay:
                fetchFromRemoteExpectation.fulfill()
            default:
                XCTFail("Failed to fetch from remote")
            }
        }
        wait(for: [fetchFromRemoteExpectation], timeout: 0.5)
        XCTAssert(mockStorage.storage.count > 0)
    }
}

class MockStorage: PersistentStorage {
    
    var storage: [String: Data] = [:]
    
    func getObjectForKey<T: Codable>(
        _ key: PersistentStorageKey,
        completion: @escaping (Result<T>) -> Void) {
        guard let data = storage[key.rawValue] else {
            completion(Result.failed(error: "not found on storage"))
            return
        }
        let result = try? JSONDecoder().decode(T.self, from: data)
        completion(Result.okay(data: result))
    }
    
    func saveObjectForKey<T: Codable>(_ key: PersistentStorageKey,
                                      object: T, completion: ((Bool) -> Void)?) {
        guard let data = try? JSONEncoder().encode(object) else {
            completion?(false)
            return
        }
        storage[key.rawValue] = data
        completion?(true)
    }
}
