//
//  TestsJsonLoaderHelper.swift
//  WunderTestTests
//
//  Created by Felipe Figueiredo on 2/12/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation
import XCTest

class TestsJsonLoaderHelper {
    static func loadJson<T: Codable>(named: String) -> T? {
        guard let url = Bundle.main.url(forResource: named, withExtension: "json") else {
            return nil
        }
        do {
            let data = try Data(contentsOf: url)
            let object = try JSONDecoder().decode(T.self, from: data)
            return object
        } catch {
            print("Error while parsing json")
            return nil
        }
    }
}
