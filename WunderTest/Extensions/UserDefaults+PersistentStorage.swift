//
//  UserDefaults+PersistentStorage.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

extension UserDefaults: PersistentStorage {
    
    func getObjectForKey<T: Codable>(_ key: PersistentStorageKey, completion: @escaping (Result<T>) -> Void) {
        guard let data = self.data(forKey: key.rawValue) else {
            completion(Result.okay(data: nil))
            return
        }
        do {
            let object = try JSONDecoder().decode(T.self, from: data)
            completion(Result.okay(data: object))
        } catch {
            completion(Result.failed(error: "Error while decoding data"))
        }
    }
    
    func saveObjectForKey<T: Codable>(_ key: PersistentStorageKey, object: T, completion: ((Bool) -> Void)?) {
        do {
            let dataFromObject = try JSONEncoder().encode(object)
            self.setValue(dataFromObject, forKey: key.rawValue)
            completion?(true)
        } catch {
            completion?(false)
        }
    }
}
