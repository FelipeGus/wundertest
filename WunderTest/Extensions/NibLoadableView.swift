//
//  Created by Felipe Figueiredo
//

import UIKit

public protocol NibLoadableView {
    static var nibName: String { get }
    static var nib: UINib { get }
}

extension NibLoadableView where Self: UIView {
    public static var nibName: String {
        return String(describing: self)
    }
    
    public static var nib: UINib {
        return UINib(nibName: self.nibName, bundle: nil)
    }
}

extension UITableViewCell: NibLoadableView {}

extension UITableViewHeaderFooterView: NibLoadableView {}

extension UICollectionReusableView: NibLoadableView {}
