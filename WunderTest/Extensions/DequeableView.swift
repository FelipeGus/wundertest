//
//  Created by Felipe Figueiredo
//

import UIKit

extension UICollectionView {
    
    /// A register for the Class-based cell
    public func registerWithClass<T: UICollectionViewCell>(_: T.Type) {
        register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    /// A register for the Nib-based cell
    public func registerWithNib<T: UICollectionViewCell>(_: T.Type) {
        register(T.nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    /// A dequeue facilitation for cells of the  collection view
    public func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Dequeing a cell with identifier: \(T.reuseIdentifier) failed.")
        }
        return cell
    }
    
    /// A register for the Class-based supplementary view
    public func registerSupplementary<T: UICollectionReusableView>(_: T.Type, kind: String) {
        register(T.self, forSupplementaryViewOfKind: kind,
                withReuseIdentifier: T.reuseIdentifier)
    }
    
    /// A register for the Nib-based supplementary view
    public func registerSuplementaryWithNib<T: UICollectionReusableView>(_: T.Type, kind: String) {
        register(T.nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: T.reuseIdentifier)
    }
    
    /// A dequeue facilitation for supplementary view of the collection view
    public func dequeueReusableView<T: UICollectionReusableView>(kind: String, atIndexPath indexPath: IndexPath) -> T {
        guard let view = dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError(" Dequeing supplementary view of kind: \( kind ) with identifier: " +
                "\( T.reuseIdentifier ) failed.")
        }
        return view
    }
}

extension UITableView {
    
    /// A register for the Class-based cell
    public func registerWithClass<T: UITableViewCell>(_: T.Type) {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    // A register for the Nib-based cell
    public func registerwithNib<T: UITableViewCell>(_: T.Type) {
        register(T.nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    /// A dequeue facilitation for the cells of table view
    public func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Dequeing a cell with identifier: \(T.reuseIdentifier) failed.")
        }
        return cell
    }
    
    /// A register for the Class-based header/footer view
    public func registerHeaderFooter<T: UITableViewHeaderFooterView>(_: T.Type) {
        register(T.self, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }
    
    // A register for the Nib-based header/footer view
    public func registerHeaderFooterWithNib<T: UITableViewHeaderFooterView>(_: T.Type) {
        register(T.self, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }
    
    /// A dequeue facilitation for reusable view of the table view 
    public func dequeueReusableView<T: UITableViewHeaderFooterView>() -> T? {
        return dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T
    }
}
