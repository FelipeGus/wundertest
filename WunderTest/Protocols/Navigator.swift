//
//  Navigator.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit

protocol Navigator: class {
    var navigationController: UINavigationController { get set }
    func didManifestNavigationIntent(_ intent: NavigationIntent)
}

extension Navigator {
    func navigateTo(viewController: UIViewController,
                    type: NavigationType,
                    animated: Bool) {
        switch type {
        case .customPresent(let style):
            viewController.modalPresentationStyle = style
            self.navigationController.present(viewController, animated: animated)
            
        case .present:
            let navController = UINavigationController(rootViewController: viewController)
            navController.isNavigationBarHidden = true
            self.navigationController.present(navController, animated: animated)
            
        case .push:
            if let navController = self.navigationController.presentedViewController as? UINavigationController {
                navController.pushViewController(viewController, animated: animated)
            } else {
                self.navigationController.pushViewController(viewController, animated: animated)
            }
        }
    }
    
    func didManifestNavigationIntent(_ intent: NavigationIntent) {
        switch intent {
        case .dismiss:
            self.navigationController.dismiss(animated: true, completion: nil)
            
        case .pop:
            if let navController = self.navigationController.presentedViewController as? UINavigationController {
                navController.popViewController(animated: true)
            } else {
                self.navigationController.popViewController(animated: true)
            }
        case .navigate(let type, let viewController, let animated):
            navigateTo(viewController: viewController, type: type, animated: animated)
        }
    }
}
