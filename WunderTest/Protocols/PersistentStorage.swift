//
//  PersistentStorageProtocol.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

protocol PersistentStorage {
    func getObjectForKey<T: Codable>(_ key: PersistentStorageKey, completion: @escaping (Result<T>) -> Void)
    func saveObjectForKey<T: Codable>(_ key: PersistentStorageKey, object: T,
                                      completion: ((Bool) -> Void)?)
}

enum PersistentStorageKey: String {
    case carsList
}
