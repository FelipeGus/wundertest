//
//  Color.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit

enum Color {
    static var colorProvider: ColorProvider = LightThemeProvider()

    case primary
    case text

    var value: UIColor {
        return Color.colorProvider.getUIColorFor(palleteItem: self)
    }
}

protocol ColorProvider {
    func getUIColorFor(palleteItem: Color) -> UIColor
}

struct LightThemeProvider: ColorProvider {
    func getUIColorFor(palleteItem: Color) -> UIColor {
        switch palleteItem {
        case .primary:
            //Green
            return UIColor(red: 157, green: 199, blue: 71)
        case .text:
            //Greenish black
            return UIColor(red: 51, green: 65, blue: 70)
        }
    }
}
