//
//  Dimensions.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit

final class Dimensions {
    
    static var height: CGFloat {
        return UIApplication.shared.keyWindow?.frame.height
            ?? UIScreen.main.bounds.height
    }
    
    static var width: CGFloat {
        return UIApplication.shared.keyWindow?.frame.width ?? UIScreen.main.bounds.width
    }
    
    static var statusHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.height
    }
}
