//
//  FontLibrary.swift
//

import Foundation
import UIKit

enum Font {
    static var fontProvider: FontSizeProvider = DefaultIphoneFontSizeProvider()
    
    enum Size {
        case huge
        case title
        case big
        case headline
        case medium
        case small
        case custom(CGFloat)
    }
    
    case regular(Size)
    case bold(Size)
    case semibold(Size)
    
    var value: UIFont {
        switch self {
        case .bold(let size):
            let sizeValue = Font.fontProvider.getSizeValue(size: size)
            return UIFont.systemFont(ofSize: sizeValue, weight: .bold)

        case .regular(let size):
            let sizeValue = Font.fontProvider.getSizeValue(size: size)
            return UIFont.systemFont(ofSize: sizeValue)
            
        case .semibold(let size):
            let sizeValue = Font.fontProvider.getSizeValue(size: size)
            return UIFont.systemFont(ofSize: sizeValue, weight: .semibold)
        }
    }
}

protocol FontSizeProvider {
    func getSizeValue(size: Font.Size) -> CGFloat
}

struct DefaultIphoneFontSizeProvider: FontSizeProvider {
    func getSizeValue(size: Font.Size) -> CGFloat {
        switch size {
        case .custom(let sizeValue):
            return sizeValue
            
        case .big:
            return 32
            
        case .headline:
            return 24
            
        case .huge:
            return 48
            
        case .medium:
            return 18
            
        case .small:
            return 16
            
        case .title:
            return 40
        }
    }
}

struct SmallIphonesFontProvider: FontSizeProvider {
    func getSizeValue(size: Font.Size) -> CGFloat {
        switch size {
        case .custom(let sizeValue):
            return sizeValue
            
        case .big:
            return 28
            
        case .headline:
            return 20
            
        case .huge:
            return 40
            
        case .medium:
            return 16
            
        case .small:
            return 14
            
        case .title:
            return 34
        }
    }
}
