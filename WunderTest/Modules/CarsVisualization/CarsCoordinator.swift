//
//  CarsCoordinator.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit

//swiftlint:disable class_delegate_protocol
protocol CarsModuleActionsDelegate: Navigator {
    func showCarsMap()
}

class CarsCoordinator: Coordinator {
    
    // MARK: - Properties
    var childCoordinators: [Coordinator] = []
    let services: Services
    
    private lazy var viewControllerFactory: CarsViewControllerFactory = {
        let factory = CarsViewControllerFactory(services: services, delegate: self)
        return factory
    }()
    
    var navigationController: UINavigationController
    
    // MARK: - Init
    
    init(navigationController: UINavigationController, services: Services) {
        self.navigationController = navigationController
        self.services = services
    }
    
    /// Starts the coordinator
    
    weak var window: UIWindow?
    
    func start() {
        LocationManager.shared.requestAuthorization()
        let viewController = viewControllerFactory.createViewControllerFor(.tabBar)
        window?.rootViewController = viewController
    }
}

extension CarsCoordinator: CarsModuleActionsDelegate {
    func showCarsMap() {
        let viewController = viewControllerFactory.createViewControllerFor(.carsMap)
        didManifestNavigationIntent(.navigate(.push, viewController, animated: true))
    }
}
