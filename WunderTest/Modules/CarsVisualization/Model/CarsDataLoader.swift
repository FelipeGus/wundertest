//
//  CarsDataLoader.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

final class CarsDataLoader: CarsDataProvider {
    
    enum Requests {
        case carsList
        
        var request: URLRequest {
            switch self {
            case .carsList:
                return URLRequest(url: URL(string: "https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json")!)
            }
        }
    }
    
    var storage: PersistentStorage?
    
    func carsInformation(completion: @escaping (Result<[Car]>) -> Void) {
        if let storage = storage {
            storage.getObjectForKey(PersistentStorageKey.carsList,
                                    completion: { [unowned self] (result: Result<StorageContainer<[Car]>>) in
                switch result {
                case .failed(let error):
                    print(error)
                    self.fetchCarsFromRemote(completion: completion)
                    
                case .okay(let data):
                    guard let container = data else {
                        self.fetchCarsFromRemote(completion: completion)
                        return
                    }
                    self.validateStoredContent(container: container, completion: { (valid) in
                        if valid {
                            completion(.okay(data: container.value))
                        } else {
                            self.fetchCarsFromRemote(completion: completion)
                        }
                    })
                }
            })
        } else {
            self.fetchCarsFromRemote(completion: completion)
        }
    }
    
    func validateStoredContent(container: StorageContainer<[Car]>, completion: @escaping (Bool) -> Void) {
        var request = Requests.carsList.request
        request.httpMethod = "HEAD"
        var httpResponse: HTTPURLResponse?
        let semaphore = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: request) { (_, response, error) in
            guard error == nil else {
                completion(false)
                return
            }
            httpResponse = response as? HTTPURLResponse
            semaphore.signal()
        }
        task.resume()
        semaphore.wait()
        let lastModified = httpResponse?.allHeaderFields["Last-Modified"] as? String
        if container.lastModifiedDate == lastModified {
            completion(true)
        }
        completion(false)
    }
    
    func fetchCarsFromRemote(completion: @escaping (Result<[Car]>) -> Void) {
        fetchCarsJson { [weak self] (data, response, error) in
            guard error == nil else {
                completion(.failed(error: error!.localizedDescription))
                return
            }
            guard let data = data else {
                completion(.failed(error: "Data not Found"))
                return
            }
            guard let placemarks = try? JSONDecoder().decode(Placemarks.self, from: data) else {
                completion(.failed(error: "Error in decoding object"))
                return
            }
            let lastModifiedDate = (response as? HTTPURLResponse)?.allHeaderFields["Last-Modified"] as? String
            let storage = StorageContainer(value: placemarks, lastModifiedDate: lastModifiedDate ?? "")
            self?.storage?.saveObjectForKey(.carsList, object: storage, completion: nil)
            completion(Result.okay(data: placemarks.cars))
        }
    }
    
    private func fetchCarsJson(completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let session = URLSession.shared
        let request = Requests.carsList.request
        let task = session.dataTask(with: request, completionHandler: completion)
        task.resume()
    }
    
}
