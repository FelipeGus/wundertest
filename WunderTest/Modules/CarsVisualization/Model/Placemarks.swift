//
//  Placemarks.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

struct Placemarks: Codable {
    let cars: [Car]
    
    enum CodingKeys: String, CodingKey {
        case cars = "placemarks"
    }
}
