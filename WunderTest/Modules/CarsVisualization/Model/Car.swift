//
//  Car.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

struct Car: Codable {
    let name: String
    let address: String
    let coordinates: [Double]
    let fuel: Int
    let vin: String
    let engineType: String
    let interior: ConservationState
    let exterior: ConservationState
    
    var coordinatesPair: LatLong {
        guard coordinates.count > 1 else { return LatLong(latitude: 0, longitude: 0) }
        return LatLong(latitude: coordinates[1], longitude: coordinates[0])
    }
    
    var longitude: Double {
        return coordinates[1]
    }
}

enum ConservationState: String, Codable {
    case unacceptable = "UNACCEPTABLE"
    case good = "GOOD"
    var statusEmoji: String {
        switch self {
        case .unacceptable:
            return "🔴"
        case .good:
            return "❇️"
        }
    }
}
