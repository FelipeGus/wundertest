//
//  CarTableViewCell.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit
import Kingfisher

class CarTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundImageView: UIImageView! {
        didSet {
            backgroundImageView.contentMode = .scaleAspectFill
            backgroundImageView.alpha = 0.3
        }
    }
    @IBOutlet weak var fuelView: FuelView!
    @IBOutlet weak var vinLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var interiorLabel: UILabel! {
        didSet {
            interiorLabel.text = R.string.localizable.interior()
        }
    }
    @IBOutlet weak var exteriorLabel: UILabel! {
        didSet {
            exteriorLabel.text = R.string.localizable.exterior()
        }
    }
    @IBOutlet weak var exteriorStateLabel: UILabel!
    @IBOutlet weak var interiorStateLabel: UILabel!
    @IBOutlet weak var upperContentView: UIView!
    @IBOutlet weak var backgroundImageViewConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        self.clipsToBounds = true
    }

    override func setHighlighted(_ highlighted: Bool,
                                 animated: Bool) {
        super.setHighlighted(highlighted, animated: true)
        guard backgroundImageView?.image != nil else { return }
        backgroundImageViewConstraint.constant = highlighted ? 300 : 160
        UIView.animate(withDuration: 0.3) {
            self.upperContentView.alpha = highlighted ? 0 : 1.0
            self.fuelView.alpha = highlighted ? 0 : 1.0
            self.backgroundImageView.alpha  = highlighted ? 1.0 : 0.3
        }
    }
    
    func configureForCar(car: Car) {
        nameLabel.text = "\(R.string.localizable.name()): \(car.name)"
        vinLabel.text = "\(R.string.localizable.vin()): \(car.vin)"
        addressLabel.text = car.address
        exteriorStateLabel.text = car.exterior.statusEmoji
        interiorStateLabel.text = car.interior.statusEmoji
        fuelView.configureFor(amount: car.fuel)
        configureImage(lat: car.coordinates[0], long: car.coordinates[1])
    }
    
    private func configureImage(lat: Double, long: Double) {
        let urlString = """
https://static-maps.yandex.ru/1.x/?lang=en-US&ll=\(lat),\(long)&z=14&l=map&size=600,300&pt=\(lat),\(long),vkbkm
"""
        guard let url = URL(string: urlString) else {
            return
        }
        backgroundImageView.kf.setImage(with: ImageResource(downloadURL: url))
    }
}
