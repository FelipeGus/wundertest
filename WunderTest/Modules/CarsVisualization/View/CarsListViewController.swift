//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.

import UIKit
import RxSwift
import RxCocoa
import Cartography

final class CarsListViewController: UIViewController {
    
    private lazy var placeholderView: PlaceholderView = {
        let placeholderView = PlaceholderView()
        placeholderView.configureFor(descriptionText: R.string.localizable.placeholderMessage())
        return placeholderView
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.color = Color.primary.value
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    
    @IBOutlet weak var tableView: FFTableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.backgroundColor = .clear
            tableView.tableFooterView = UIView()
            tableView.separatorStyle = .none
            tableView.estimatedRowHeight = 100
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.registerwithNib(CarTableViewCell.self)
            tableView.placeholderView = placeholderView
        }
    }
    
    var viewModel: CarsListViewModel!
    var disposeBag: DisposeBag = DisposeBag()

    // MARK: - Init

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        LocationManager.shared.startUpdatingLocations()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        LocationManager.shared.stopUpdatingLocations()
    }
    // MARK: - View Configuration Methods

    func setupInitialState() {
        bindViewModel()
        configureLargeTitle()
        configureSearchController()
        setupActivityIndicator()
        setupPlaceholderView()
    }
    
    func setupPlaceholderView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(reload))
        placeholderView.addGestureRecognizer(tapGesture)
    }
    
    @objc func reload() {
        placeholderView.isHidden = true
        activityIndicator.startAnimating()
        viewModel.fetchCarsInformation()
    }
    
    func setupActivityIndicator() {
        self.view.addSubview(activityIndicator)
        constrain(activityIndicator) { (indicator) in
            indicator.centerY == indicator.superview!.centerY - 50
            indicator.centerX == indicator.superview!.centerX
        }
        activityIndicator.startAnimating()
    }

    private func configureLargeTitle() {
        title = R.string.localizable.carList()
        let navBar = navigationController?.navigationBar
        navBar?.prefersLargeTitles = true
        navBar?.isTranslucent = true
        self.definesPresentationContext = true
    }
    
    private var searchController: UISearchController?
    private func configureSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController?.dimsBackgroundDuringPresentation = false
        searchController?.searchResultsUpdater = self
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
    }
}

// MARK: - VC -- VM Binding

extension CarsListViewController {
    func bindViewModel() {
        viewModel.state.asDriver()
            .drive( onNext: { [weak self] (newState) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1,
                                              execute: {
                                                self?.activityIndicator.stopAnimating()
                })
                switch newState {
                case .loading:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        self?.activityIndicator.startAnimating()
                    })

                case .failed(let error):
                    self?.tableView.readyToShowPlaceholder = true
                    self?.placeholderView.configureFor(descriptionText: error)
                    self?.tableView.reloadData()
                    
                case .success:
                    self?.tableView.reloadData()
                    
                default:
                    break
                }
            }).disposed(by: disposeBag)
    }
}

// MARK: - TableViewDataSource

extension CarsListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if viewModel.numberOfRows() == 0 {
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CarTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let configMethod = cell.configureForCar
        viewModel.provideCar(indexPath: indexPath, for: configMethod)
        return cell
    }
}

// MARK: - TableViewDelegate

extension CarsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}

extension CarsListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text else { return }
        viewModel.query = query
    }
}
