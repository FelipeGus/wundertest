//
//  CarsViewControllerFactory.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit

class CarsViewControllerFactory {
    
    enum Blueprint {
        case carsList
        case carsMap
        case tabBar
    }
    
    let services: Services
    weak var delegate: CarsModuleActionsDelegate?
    
    init(services: Services, delegate: CarsModuleActionsDelegate) {
        self.services = services
        self.delegate = delegate
    }
    
    func createViewControllerFor(_ blueprint: Blueprint) -> UIViewController {
        switch blueprint {
        case .tabBar:
            let tabBarController = UITabBarController()
            let viewControllers = [Blueprint.carsList, .carsMap].map(createViewControllerFor)
            tabBarController.setViewControllers(
                viewControllers.map({
                    if $0 is CarsMapViewController { return $0 }
                    return UINavigationController(rootViewController: $0)
                }),
                animated: false
            )
            return tabBarController
            
        case .carsList:
            let viewController = R.storyboard.cars.carsList()!
            viewController.tabBarItem.image = R.image.menuIconTab()
            let dataLoader: CarsDataLoader = services.getServiceProvider()!
            let viewModel = CarsListViewModel(dataProvider: dataLoader)
            viewController.viewModel = viewModel
            viewController.title = R.string.localizable.carList()
            viewModel.delegate = delegate
            return viewController
            
        case .carsMap:
            let viewController = CarsMapViewController()
            viewController.tabBarItem.image = R.image.mapIconTab()
            let dataLoader: CarsDataLoader = services.getServiceProvider()!
            let viewModel = CarsMapViewModel(dataProvider: dataLoader)
            viewController.viewModel = viewModel
            viewController.title = R.string.localizable.map()
            viewModel.delegate = delegate
            return viewController
        }
    }
}
