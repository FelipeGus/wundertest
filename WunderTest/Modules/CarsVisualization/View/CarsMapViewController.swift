//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.

import UIKit
import RxSwift
import RxCocoa
import MapKit
import Cartography

final class CarsMapViewController: UIViewController {
    
    var viewModel: CarsMapViewModel!
    private var disposeBag: DisposeBag = DisposeBag()
    
    private lazy var statusOverlayEffectView: UIVisualEffectView = {
        let view = UIVisualEffectView(effect: UIBlurEffect.init(style: .extraLight))
        return view
    }()
    
    private lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        return mapView
    }()
    
    // MARK: - Init
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = R.string.localizable.map()
        setupInitialState()
    }
    
    // MARK: - View Configuration Methods
    
    func setupInitialState() {
        setupMapView()
        setupScaleView()
        setupEffectsView()
        bindViewModel()
    }
    
    private func setupEffectsView() {
        view.addSubview(statusOverlayEffectView)
        constrain(statusOverlayEffectView) { (effectView) in
            effectView.top == effectView.superview!.top
            effectView.right == effectView.superview!.right
            effectView.left == effectView.superview!.left
            effectView.height == Dimensions.statusHeight
        }
    }
    
    private func setupScaleView() {
        let scaleView = MKScaleView(mapView: mapView)
        scaleView.legendAlignment = .trailing
        view.addSubview(scaleView)
    }
    
    private func setupMapView() {
        view.addSubview(mapView)
        constrain(mapView) { (mapView) in
            mapView.edges == mapView.superview!.edges
        }
        mapView.isRotateEnabled = false
        mapView.showsUserLocation = true
        mapView.register(CarMarkerView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        mapView.register(CarsClusterAnnotationView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        mapView.delegate = self
        if let location = LocationManager.shared.coordinates {
            mapView.setRegion(MKCoordinateRegion(
                center: location.clObject,
                span: .init(latitudeDelta: 1, longitudeDelta: 1)), animated: true)
        }
    }
}

// MARK: - VC -- VM Binding

extension CarsMapViewController {
    func bindViewModel() {
        viewModel.state.asDriver()
            .drive( onNext: { [weak self] (newState) in
                switch newState {
                case .success:
                    self?.mapView.addAnnotations(self?.viewModel.carsAnnotation ?? [])
                default:
                    break
                }
            }).disposed(by: disposeBag)
    }
}

// MARK: - MKMapViewDelegate

extension CarsMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation)
        -> MKAnnotationView? {
            if annotation is MKUserLocation {
                return nil
            }
            if annotation is MKClusterAnnotation {
                return nil
            }
            let view = CarMarkerView(annotation: annotation, reuseIdentifier: "CarMarkerView")
            return view
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard !(view.annotation is MKClusterAnnotation) else { return }
        let annotation = view.annotation as? CarAnnotation
        let toRemove = viewModel.carsAnnotation.filter({ $0.car.vin != annotation?.car.vin })
        mapView.removeAnnotations(toRemove)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        mapView.removeAnnotations([view.annotation!])
        mapView.addAnnotations(viewModel.carsAnnotation)
    }
}
