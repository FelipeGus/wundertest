//
//  FuelView.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit
import Cartography

class FuelView: UIView {
    
    lazy var amountLabel: UILabel = {
        let label =  UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = Font.semibold(Font.Size.small).value
        return label
    }()

    lazy var backgroundView: UIView = {
        let view = UIView()
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInitialState()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInitialState()
    }
    
    private var rightConstraint: NSLayoutConstraint?
    private func setupInitialState() {
        addSubview(backgroundView)
        addSubview(amountLabel)
        constrain(backgroundView, amountLabel) { (background, label) in
            background.top == background.superview!.top
            background.left == background.superview!.left
            rightConstraint = background.right == background.superview!.right ~ 900
            background.bottom == background.superview!.bottom
            label.edges == label.superview!.edges
        }
    }
    
    func configureFor(amount: Int) {
        amountLabel.text = "\(R.string.localizable.fuelAmount()): \(amount)\\100"
        let offsetFromRight = (1 - CGFloat(amount)/100) * self.frame.width
        rightConstraint?.constant = -offsetFromRight
        backgroundView.backgroundColor = FuelView.colorForFuel(amount: amount)
    }
    
    class func colorForFuel(amount: Int) -> UIColor {
        let green = 64 + CGFloat(amount)/100 * 128
        let red = 255 - CGFloat(amount)/100 * 128
        return UIColor.init(red: red, green: green, blue: 0)
    }
}
