//
//  CarMarker.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/13/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation
import MapKit

class CarAnnotation: NSObject, MKAnnotation {
    var car: Car
    
    init(car: Car) {
        self.car = car
        super.init()
    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: car.coordinatesPair.latitude,
                                      longitude: car.coordinatesPair.longitude)
    }
    
    var title: String? {
        return "\(car.name)"
    }
    
    var subtitle: String? {
        return "\(R.string.localizable.fuelAmount()): \(car.fuel)%"
    }
}

class CarMarkerView: MKMarkerAnnotationView {
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        clusteringIdentifier = "Cars"
        displayPriority = .defaultLow
        glyphTintColor = Color.text.value
        glyphImage = R.image.carMarker()
        canShowCallout = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var annotation: MKAnnotation? {
        didSet {
            if let annotation = annotation as? CarAnnotation {
                markerTintColor = FuelView.colorForFuel(amount: annotation.car.fuel)
                let label = UILabel()
                label.text = annotation.subtitle!
                label.numberOfLines = 0
                detailCalloutAccessoryView = label
            }
        }
    }
}
