//
//  CarClusterView.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/14/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation
import MapKit

class CarsClusterAnnotationView: MKMarkerAnnotationView {
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        collisionMode = .circle
        centerOffset = CGPoint(x: 0, y: -10)
        markerTintColor = Color.primary.value
        glyphTintColor = .black
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForDisplay() {
        super.prepareForDisplay()
    }
    
}
