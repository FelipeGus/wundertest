//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.

import Foundation
import RxSwift
import RxCocoa
import CoreLocation

final class CarsListViewModel {

    weak var delegate: CarsModuleActionsDelegate?
    var state: BehaviorRelay<DefaultState> = BehaviorRelay(value: .begin)
    var dataProvider: CarsDataProvider
    var locationProvider: LocationProvider
    
    init(dataProvider: CarsDataProvider, locationProvider: LocationProvider = LocationManager.shared) {
        self.dataProvider = dataProvider
        self.locationProvider = locationProvider
        fetchCarsInformation()
        registerToReceiveUpdatesOfLocation()
    }

    private var cars: [Car] = []
    private var filteredCars: [Car] = []
    
    func numberOfRows() -> Int {
        return filteredCars.count
    }
    
    var query: String? {
        didSet {
            filterData()
        }
    }
    
    var observationToken: NSObjectProtocol?
    func registerToReceiveUpdatesOfLocation() {
        observationToken = NotificationCenter.default.addObserver(
            forName: LocationManager.updateLocationNotification,
            object: nil, queue: .main) { [weak self] (_) in
                guard self?.cars.count ?? 0 > 0 else { return }
                self?.orderByLocation()
                self?.state.accept(State.success(subState: ""))
        }
    }
    
    func filterData() {
        guard let query = query, !query.isEmpty else {
            self.filteredCars = cars
            orderByLocation()
            self.state.accept(State.success(subState: ""))
            return
        }
        let qLowercased = query.lowercased()
        filteredCars = cars.filter({
            $0.name.lowercased().contains(qLowercased) ||
            $0.address.lowercased().contains(qLowercased)
        })
        orderByLocation()
        self.state.accept(State.success(subState: ""))
    }
    
    func orderByLocation() {
        if let location = locationProvider.coordinates {
            let distanceFCurried = curry(HaversineDistanceHelper().getDistanceBetweenTwoPoints)
            let distanceToMyPosition = distanceFCurried(location)
            filteredCars.sort(by: { (car1, car2) -> Bool in
                let distanceToCar1 = distanceToMyPosition(car1.coordinatesPair)
                let distanceToCar2 = distanceToMyPosition(car2.coordinatesPair)
                return distanceToCar1 < distanceToCar2
            })
        }
    }
    
    func provideCar(indexPath: IndexPath, for function: ((Car) -> Void)?) {
        guard indexPath.row < filteredCars.count else { return }
        let car = filteredCars[indexPath.row]
        function?(car)
    }
    
    func fetchCarsInformation() {
        self.state.accept(.loading)
        dataProvider.carsInformation { [weak self] (result) in
            switch result {
            case .failed(let error):
                self?.state.accept(State.failed(error: error))
            case .okay(let data):
                self?.cars = data ?? []
                self?.filterData()
            }
        }
    }
    
    deinit {
        observationToken = nil
    }
    
}
