//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.

import Foundation
import RxSwift
import RxCocoa

final class CarsMapViewModel {

    weak var delegate: CarsModuleActionsDelegate?
    var state: BehaviorRelay<DefaultState> = BehaviorRelay(value: .begin)
    var dataProvider: CarsDataProvider
    
    init(dataProvider: CarsDataProvider) {
        self.dataProvider = dataProvider
        fetchCarsInformation()
    }
    
    private var cars: [Car] = []
    var carsAnnotation: [CarAnnotation] = []
    
    func fetchCarsInformation() {
        dataProvider.carsInformation { [weak self] (result) in
            switch result {
            case .failed(let error):
                self?.state.accept(State.failed(error: error))
            case .okay(let data):
                self?.cars = data ?? []
                self?.carsAnnotation = self?.cars.map({ CarAnnotation(car: $0) }) ?? []
                self?.state.accept(State.success(subState: ""))
            }
        }
    }
}
