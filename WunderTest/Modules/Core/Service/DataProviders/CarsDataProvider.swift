//
//  CarsDataProvider.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

protocol CarsDataProvider: ServiceProvider {
    func carsInformation(completion: @escaping (Result<[Car]>) -> Void)
}

extension CarsDataProvider {
    static var serviceType: ServiceType { return .carsAPI }
}
