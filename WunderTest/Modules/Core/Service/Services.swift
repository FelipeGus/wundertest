//
//  Services.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

enum ServiceType {
    case carsAPI
}

protocol ServiceProvider {
    static var serviceType: ServiceType { get }
}

protocol ServiceFactory {
    func createService(type: ServiceType) -> ServiceProvider
}

/// Services class is a container that will create a single instance of each
/// serviceType provided in its init and store it for later reuse.
class Services {
    
    private var servicesDic: [ServiceType: Any]
    
    init(serviceTypes: [ServiceType], factory: ServiceFactory) {
        self.servicesDic = [:]
        
        for type in serviceTypes {
            servicesDic[type] = factory.createService(type: type)
        }
    }
    
    private init(servicesDic: [ServiceType: Any]) {
        self.servicesDic = servicesDic
    }
    
    func containingOnly(types: [ServiceType]) -> Services {
        let servicesFiltered = servicesDic.filter({ types.contains($0.key)})
        return Services(servicesDic: servicesFiltered)
    }
    
    func getServiceProvider<T: ServiceProvider>() -> T? {
        return servicesDic[T.serviceType] as? T
    }
}
