//
//  ProductionServiceFactory.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

struct ProductionServiceFactory: ServiceFactory {
    
    init() {
    }
    
    func createService(type: ServiceType) -> ServiceProvider {
        switch type {
        case .carsAPI:
            let dataLoader = CarsDataLoader()
            dataLoader.storage = UserDefaults.standard
            return dataLoader
        }
    }
}
