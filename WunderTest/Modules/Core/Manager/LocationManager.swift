//
//  LocationManager.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationProvider {
    var coordinates: LatLong? { get }
}

class LocationManager: NSObject, CLLocationManagerDelegate, LocationProvider {
    
    static var updateLocationNotification: Notification.Name = Notification.Name.init("locations_updated")
    static var shared: LocationManager = LocationManager()
    
    private var clLocationManager: CLLocationManager
    private var lastCoordinates: CLLocationCoordinate2D?
    
    var coordinates: LatLong? {
        guard let lastCoordinates = lastCoordinates else { return nil }
        return LatLong(latitude: lastCoordinates.latitude, longitude: lastCoordinates.longitude)
    }
    
    private override init() {
        clLocationManager = CLLocationManager()
        super.init()
        clLocationManager.delegate = self
        clLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        clLocationManager.activityType = .other
        clLocationManager.distanceFilter = 10
        clLocationManager.startUpdatingLocation()
    }
    
    // MARK: - Public interface
    
    func requestAuthorization() {
        clLocationManager.requestWhenInUseAuthorization()
    }
    
    func requestUpdateLocation() {
        clLocationManager.requestLocation()
    }
    
    // MARK: - Delegate fuctions
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error) {
        print(#function)
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        lastCoordinates = nil
    }
    
    private var startUpdatingWorkItem: DispatchWorkItem?
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        self.lastCoordinates = manager.location?.coordinate
        manager.stopUpdatingLocation()
        startUpdatingWorkItem?.cancel()
        startUpdatingWorkItem = DispatchWorkItem {
            manager.startUpdatingLocation()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: startUpdatingWorkItem!)
        NotificationCenter.default.post(name: LocationManager.updateLocationNotification, object: nil)
    }
    
    func stopUpdatingLocations() {
        startUpdatingWorkItem?.cancel()
        clLocationManager.stopUpdatingLocation()
    }
    
    func startUpdatingLocations() {
        clLocationManager.startUpdatingLocation()
    }
}
