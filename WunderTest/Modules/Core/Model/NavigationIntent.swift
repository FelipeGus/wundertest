//
//  NavigationIntent.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit

enum NavigationType {
    case push
    case present
    case customPresent(style: UIModalPresentationStyle)
}

enum NavigationIntent {
    case pop
    case dismiss
    case navigate(NavigationType, UIViewController, animated: Bool)
}
