//
//  State.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

enum State<T, E> {
    case begin
    case loading
    case failed(error: E)
    case success(subState: T)
}

typealias DefaultState = State<String, String>
