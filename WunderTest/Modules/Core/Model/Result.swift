//
//  APIResult.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

enum Result<Element> {
    case okay(data: Element?)
    case failed(error: String)
}
