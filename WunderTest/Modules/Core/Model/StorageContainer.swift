//
//  StorageContainer.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

struct StorageContainer<T: Codable>: Codable {
    var value: T?
    var lastModifiedDate: String
    
    init(value: T, lastModifiedDate: String) {
        self.value = value
        self.lastModifiedDate = lastModifiedDate
    }
}
