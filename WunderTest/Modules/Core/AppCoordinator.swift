//
//  AppCoordinator.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class AppCoordinator: Coordinator {
    
    // MARK: - Properties
    var childCoordinators: [Coordinator] = []
    var services: Services
    
    // MARK: - Window Management
    
    /// Window to manage
    let window: UIWindow
    
    private lazy var navigationController: UINavigationController = {
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        return navigationController
    }()
    
    // MARK: - Init
    
    init(window: UIWindow, services: Services) {
        self.window = window
        self.services = services
        self.window.rootViewController = self.navigationController
        self.window.makeKeyAndVisible()
    }
    
    private func configureGeneralAppearance() {
        window.tintColor = Color.primary.value
    }
    
    private func configureKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarTintColor = Color.primary.value.withAlphaComponent(0.5)
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    }
    
    // MARK: - Functions
    
    /// Starts the coordinator
    
    func start() {
        configureGeneralAppearance()
        configureKeyboard()
        let carsCoordinator = CarsCoordinator(
            navigationController: navigationController,
            services: services.containingOnly(types: [.carsAPI])
        )
        carsCoordinator.window = window
        carsCoordinator.start()
    }
}
