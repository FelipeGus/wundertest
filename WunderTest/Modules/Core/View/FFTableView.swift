//
//  FFTableView.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit

public class FFTableView: UITableView {
    
    /// ## Placeholder Display behavior
    /// - **showAlways**: placeholder will always display when the dataSource returns 0 as the number of sections
    /// - **skipFirstLoad**: placeholder will display according to
    /// the number of sections after the first reload data is called.
    public enum PlaceholderDisplayBehavior {
        case showAlways
        case skipFirstLoad
    }
    
    /// Any View that might serve as a placeholder view.
    /// The TableView will centralize this view within it's frame.
    public var placeholderView: UIView? = nil {
        willSet {
            placeholderView?.removeFromSuperview()
        }
        didSet {
            configurePlaceholderView()
        }
    }
    
    public var readyToShowPlaceholder: Bool = false
    
    //Placeholder Diplay Behavior controls when the placeholder view you used for placeholder will appear.
    public var placeholderDisplayBehavior: PlaceholderDisplayBehavior = .showAlways
    
    private var currentNumberOfReloads: Int = 0
    
    // MARK: - Inits
    
    public override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Configuration Methods
    
    private func configurePlaceholderView() {
        guard let placeholderView = placeholderView else { return }
        self.addSubview(placeholderView)
        placeholderView.translatesAutoresizingMaskIntoConstraints = false
        placeholderView.centerXAnchor
            .constraint(equalTo: centerXAnchor).isActive = true
        placeholderView.centerYAnchor
            .constraint(equalTo: centerYAnchor).isActive = true
        placeholderView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        placeholderView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        placeholderView.isHidden = true
    }
    
    // MARK: - Overload of TableView Methods
    
    public override func reloadData() {
        currentNumberOfReloads += 1
        if let dataSource = dataSource,
            let sections = dataSource.numberOfSections?(in: self) {
            var placeholderViewIsHidden = sections > 0
            if sections == 1 {
                placeholderViewIsHidden = dataSource.tableView(
                    self, numberOfRowsInSection: 0) > 0
                    || !readyToShowPlaceholder
            }
            switch placeholderDisplayBehavior {
            case .showAlways:
                placeholderView?.isHidden = placeholderViewIsHidden
                
            case .skipFirstLoad:
                if currentNumberOfReloads > 1 {
                    placeholderView?.isHidden = placeholderViewIsHidden
                } else {
                    placeholderView?.isHidden = true
                }
            }
        }
        super.reloadData()
    }
}
