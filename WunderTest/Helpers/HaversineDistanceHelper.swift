//
//  HaversineDistanceHelper.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import UIKit

struct HaversineDistanceHelper {
    
    func getDistanceBetweenTwoPoints(po1: LatLong, po2: LatLong) -> Double {
        let radius = 6371.0
        let dLat = deg2rad(po2.latitude - po1.latitude)
        let dLon = deg2rad(po2.longitude - po1.longitude)
        let arc = pow(sin(dLat/2), 2) + pow(sin(dLon/2), 2) *
                cos(deg2rad(po1.latitude)) * cos(deg2rad(po2.latitude))
        let cAngle = 2 * atan2(sqrt(arc), sqrt(1 - arc))
        let distance = radius * cAngle
        return distance
    }
    
    func deg2rad(_ degrees: Double) -> Double {
        return degrees * (Double.pi/180.0)
    }
}
