//
//  FunctionalProgrammingHelper.swift
//  WunderTest
//
//  Created by Felipe Figueiredo on 2/10/19.
//  Copyright © 2019 Felipe Figueiredo. All rights reserved.
//

import Foundation

//Operator to Pipe curried functions output

infix operator |||: AdditionPrecedence

func ||| <A,B,C>(f: @escaping (A) -> B, g: @escaping (C) -> A) -> (C) -> B {
    return { x in
        f(g(x))
    }
}

func ||| <A,B,C,D>(f: @escaping (A) -> B, g: @escaping (C) -> (D) -> A) -> (C) -> (D) -> B {
    return { x in
        return { y in
            return f(g(x)(y))
        }
    }
}

func ||| <A,B,C,D,E>(f: @escaping (A) -> B, g: @escaping (C) -> (D) -> (E)-> A) -> (C) -> (D) -> (E) -> B {
    return { x in
        return { y in
            return { z in
                return f(g(x)(y)(z))
            }
        }
    }
}

func ||| <A,B,C,D,E,F>(
    f: @escaping (A) -> B,
    g: @escaping (C) -> (D) -> (E)-> (F) -> A
    ) -> (C) -> (D) -> (E) -> (F) -> B
{
    return { x in
        return { y in
            return { z in
                return { k in
                    return f(g(x)(y)(z)(k))
                }
            }
        }
    }
}

//Operator to compose functions

infix operator |>: AdditionPrecedence

func |> <A,B,C>(f: @escaping (A) -> B, g: @escaping (B) -> C) -> (A) -> C {
    return { x in
        g(f(x))
    }
}

// Function that curries a function of 2 parameters

func curry<A, B, C> (_ f: @escaping (A, B) -> C) -> (A) -> (B) -> C {
    return { (a: A) -> (B) -> C in
        { (b: B) in
            return f(a,b)
        }
    }
}

// Usually sensible functions have up to 4 parameters, so the functions for those would be

func curry<A,B,C,D>(_ f: @escaping (A,B,C) -> D) -> (A) -> (B) -> (C) -> (D) {
    return { (a: A) -> (B) -> (C) -> D in
        return { (b: B) -> (C) -> D in
            return { (c: C) -> D in
                return f(a,b,c)
            }
        }
    }
}

func curry<A,B,C,D,E>(_ f: @escaping (A,B,C,D) -> E) -> (A) -> (B) -> (C) -> (D) -> E {
    return { (a: A) -> (B) -> (C) -> (D) -> E in
        return { (b: B) -> (C) -> (D) -> E in
            return { (c: C) -> (D) -> E in
                return { (d: D) -> (E) in
                    return f(a,b,c,d)
                }
            }
        }
    }
}
